#! /bin/bash
 
sent_user()
{
    i=1
    j=1
    while [ -s "./$1-$i" ]
        do
            while [ -s "./$1-$j" ]
                do
                    if [ "$j" != "$i" ]
                        then
                        if [ "$(cat ./$1-$j)" = "$(cat ./$1-$i)" ]
                            then
                            mail -s "Urgent" $1 < $2
                            return
                        fi
                    fi
                    ((j=j+1))
            done
            ((i=i+1))
            ((j=i))
    done
}
 
if [ -z "$1" ]
    then
    echo "There are no arguments!"
    exit
fi
 
READFILE="$1"

if [ ! -s "$READFILE" ]
    then
    echo "$READFILE is not a file" 
    exit
fi

if [ ! -s $MAIL ]
    then
    echo "Your postbox is empty"
    exit
fi

i=0
k=0
while read a
    do
        ((k=k+1))
        if [ "${a:0:5}" = "From " ]
        then
            ((k=0))
            ((i=i+1))
        fi
        if [ "$k" -eq "11" ]
            then
            USER_NAME=${a:15:8}
            echo "./$USER_NAME-$i" >> ~/FROM
        fi
        if [ "$k" -gt "12" ]
            then
            echo "$a" >> ~/$USER_NAME-$i
        fi
done < $MAIL
tem=1
sim=0
while IFS= read -r row
    do
        while IFS= read -r line
            do
                if [ "$cat" != "$row" ]
                    then
                    if [ "$(cat $line)" = "$(cat $row)" ]
                        then
                        ((sim=sim+1))
                    fi

                fi
        done < FROM
        if [ "$sim" -ge "$tem" ]
            then
            ((tem=sim))
            ((sim=0))
        fi
done < FROM
j=0
while IFS= read -r line
    do
        if [ "$j" -eq "0" ]
            then
            USER=${line:2:8}
            ((j=j+1))
            sent_user $USER $READFILE
            continue
        fi
        if [ "${line:2:8}" != "$USER" ]
            then
            ((USER=${line:2:8}))
            sent_user $USER $READFILE
        fi
done < FROM
 
if [ "$tem" -ge "1" ]
    then
    echo "You have $tem similar letters!"
    else
        echo "You doesn't have similar letters!"
fi